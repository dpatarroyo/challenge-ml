import React from 'react';

const Suggestions = (props: any) => {
    const [suggestions] = React.useState(props.suggestions);
    const [title] = React.useState(props.title);

    return (
        <nav>
            <ul className="list-none flex flex-wrap gap-2 text-sm mt-1 mb-6">
                <li className="font-[700]">{title}</li>
                {suggestions.map((suggestion: any, i: number) => {
                    return <li key={i}>{suggestion.q} {i < suggestions.length - 1 &&
                        <span className="ml-1">-</span>}</li>
                })}
            </ul>
        </nav>
    );
};

export default Suggestions;