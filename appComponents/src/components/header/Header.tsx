import './header.scss'
import React, {useEffect, useState} from 'react'
import {Input, Navbar, NavbarBrand, NavbarContent, NavbarMenuToggle} from "@nextui-org/react";
import NavMenu from "../nav-menu/NavMenu";
import {useLocation, useSearch} from "wouter";
const Header = (props: any) => {
    const [data] = useState(props);
    const [showDrawer, setShowDrawer] = React.useState(false);
    const [query, setQuery] = useState<string>('');
    const [location, navigate] = useLocation();
    const search = useSearch()

    useEffect(() => {
        const _search:string = search.replaceAll('-', ' ').split('=')[1];
        if (!query && _search) {
            setQuery(_search);
        }
    }, [location]);

    const searchItem = () => {
        let queryParam = query.trim().normalize('NFD').replace(/[\u0300-\u036f]/g,"").replaceAll(' ', '-');
        if (queryParam.length > 0) {
            navigate(`/items?search=${queryParam}`);
            window.dispatchEvent(new Event('changeSearch'));
        }
    };

    return(
        <Navbar className="w-full m-0 p-0 nav-header relative z-10" position="static" maxWidth={"xl"} height={"auto"} onMenuOpenChange={(isSelected:boolean) => { setShowDrawer(isSelected)}}>
            <NavbarContent className="w-full mx-auto m-0 py-2 sm:px-3 flex items-start flex-wrap">
                <NavbarMenuToggle className="sm:hidden w-4 h-4 mt-4 z-20 toggle-nav"/>
                <NavbarBrand className="w-full sm:block flex justify-center md:max-w-[154px]">
                    <img className="cursor-pointer logo ml-[-34px] sm:ml-[0px]" src={data.logo} alt="merdadolibre" onClick={() => { navigate('/') }}/>
                </NavbarBrand>
                <div className="flex flex-wrap w-full h-auto gap-1 sm:max-w-[82%]">
                    <Input
                        classNames={{
                            base: "h-10 lg:max-w-[588px] sm:max-w-[70%]",
                            mainWrapper: "h-full",
                            input: "text-small",
                            inputWrapper: "h-full font-normal text-default-500 rounded-none",
                        }}
                        onChange={(e) => setQuery(e.target.value)}
                        placeholder="Buscar productos, marcas y más…"
                        onKeyUp={(e) => e.key === 'Enter' && searchItem()}
                        value={query}
                        endContent={<div className="cursor-pointer mr-[-11px] h-full w-[33px] flex justify-center items-center" onClick={() => { searchItem() }}><i className="las la-search"></i></div>}
                        type="search"
                    />

                    <NavMenu showDrawer={showDrawer} menu={data.menu}></NavMenu>

                </div>
            </NavbarContent>
        </Navbar>
    )
}
export default Header;