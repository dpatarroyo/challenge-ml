import {useState} from "react";

export const useLocationSearch = () => {
    const [search, setSearch] = useState(window.location.search.replaceAll('-', ' ').split('=')[1]);
    window.addEventListener('changeSearch', function () {
        let newQuery = window.location.search.replaceAll('-', ' ').split('=')[1];
        if (newQuery && newQuery !== search) setSearch(newQuery);
    });
    return [search, setSearch];
};