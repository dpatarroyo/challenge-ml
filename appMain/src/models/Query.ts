export class Query {
    query: string;
    siteId: string;
    sort: any;
    paging: any;
    filters: any;
    constructor(query?: string, sort?: any, paging?: any, siteId?: string, filters?: any) {
        this.query = query ?? '';
        this.siteId = siteId ?? 'MCO';
        this.sort = sort ?? { id: 'relevance', name: 'Más relevantes' }
        this.paging = paging ?? {
            limit: 15,
            offset: 0,
            total: 0,
            results: 0
        };
        this.filters = filters ?? [];
    }
}