import React from 'react';
import {Link} from "wouter";

const NotFound = () => {
    return (
        <div className="w-full h-full flex justify-center items-center">
            <div className="w-72 text-center">
                <img className="mx-auto mb-5" src={require('../assets/images/404.svg')} alt="404"/>
                <h4 className="font-bold text-lg mb-4">Parece que esta página no existe</h4>
                <Link className="text-sm" to="/">Ir a la página principal</Link>
            </div>
        </div>
    );
};

export default NotFound;