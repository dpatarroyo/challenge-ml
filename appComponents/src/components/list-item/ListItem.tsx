import React, {useState} from 'react';
import {Card, CardBody, Image} from "@nextui-org/react";
import {Rating} from "react-simple-star-rating";
import {Link} from "wouter";

const ListItem = (prop: any) => {
    const [item] = useState(prop.item);
    const [util] = useState(prop.util);
    const [fullLogo] = useState(prop.fullLogo);

    return (
        <Card radius="none" shadow="none" className="border-b-1">
            <CardBody className="p-[11px] lg:py-[20px] lg:pl-0 lg:pr-[50px]">
                <div className="flex flex-wrap gap-2">
                    <figure className="w-full lg:w-[208px] flex justify-center items-center">
                        <Link to={`/items/${item.id}`}>
                            <Image
                                radius="none"
                                width={160}
                                height={160}
                                src={util.webpThumbnail(item.thumbnail)}
                                alt="NextUI hero Image"
                            />
                            {/*<img className="w-[160px] h-[160px] object-center object-cover mx-auto" src={util.webpThumbnail(item.thumbnail)} alt="item"/>*/}
                        </Link>
                    </figure>
                    <section className="w-full flex-1 text-center lg:text-left">
                    <h2 className="text-xl font-[300] mt-2 leading-5">{item.title}</h2>
                        {item.official_store_name &&
                        <span className="text-xs font-[300] text-ter block">Por {item.official_store_name}</span>
                        }
                        <div className="grid grid-cols-2 gap-2 mt-3">
                            <div className="col-span-2 md:col-span-1">
                                { item.original_price &&
                                <span className="text-xs font-[300] text-ter line-through block">{util.currency(item.original_price)}</span>
                                }
                                <span className="text-[400] font-[400] text-[24px]">{util.currency(item.price)}</span>
                                { item.original_price && <span className="ml-2 text-succ text-[14px] font-[400]">{util.getDiscount(item.price, item.original_price)}</span>}

                                {item.installments && item.installments.quantity &&
                                    <span className="block text-[14px]">en {item.installments.quantity}x {util.currency(item.installments.amount)}</span>
                                }
                                {item.shipping.free_shipping &&
                                    <span className="block mt-3 text-succ text-[14px] font-[600]">Envío gratis</span>
                                }
                                {item.shipping.logistic_type == 'fulfillment' &&
                                    <span className="block mt-3 text-sec text-[14px] font-[400] flex">Envíado por
                                        <img className="cursor-pointer w-[45px] ml-1" src={fullLogo} alt="envio-full"/>
                                    </span>
                                }
                            </div>
                            <div className="mt-3 col-span-2 md:col-span-1">
                                <span
                                    className="font-[300] text-[14px] mr-2">{item.rate = Math.round(Math.random() * (5))}</span>
                                <span>
                                    <Rating className="mb-2" emptyColor="#fff" fillColor='#3483fa' SVGstorkeWidth={1}
                                            SVGstrokeColor="#3483fa" size={16} disableFillHover={true} readonly={true}
                                            initialValue={item.rate}/>
                                </span>
                                <span
                                    className="font-[300] text-[14px] ml-2">({Math.round(Math.random() * (500))})
                                </span>
                            </div>
                        </div>
                    </section>
                </div>
            </CardBody>
        </Card>
    );
};

export default ListItem;