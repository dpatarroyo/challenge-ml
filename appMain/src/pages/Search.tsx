import React, {Key, useContext, useEffect, useState} from 'react'
import { GlobalLoaderContext } from "../App";
import { http } from "../api/http";
import ListItem from "components/ListItem";
import Suggestions from "components/Suggestions";
import { useLocationSearch } from "../util/hooks";
import * as util from "../util/utils";
import {
    Card,
    CardBody,
    cn,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownTrigger,
    Pagination,
    Switch
} from "@nextui-org/react";
import {Query} from "../models/Query";
import {join} from "../util/utils";


const STORES_IMG: any = [
    'D_NQ_NP_978072-MLA27130853286_042018-A.webp',
    'D_NQ_NP_841110-MLA27927033182_082018-A.webp',
    'D_NQ_NP_674161-MLA28341673656_102018-A.webp',
    'D_NQ_NP_704693-MLA28341646754_102018-A.webp',
    'D_NQ_NP_655247-MLA27129115788_042018-A.webp',
    'D_NQ_NP_931307-MLA31050819295_062019-A.webp'
]


const checkInit: any = {
    shipping: {
        show: false,
        checked: false,
        filter: { id: 'shipping', values: [{ id: 'fulfillment' }]}
    },
    shipping_cost: {
        show: false,
        checked: false,
        filter: { id: 'shipping_cost', values: [{ id: 'free' }]}
    },
    SHIPPING_ORIGIN: {
        show: false,
        checked: false,
        filter: { id: 'SHIPPING_ORIGIN', values: [{ id: '10215069' }]}
    },
}

const Search = () => {
    const global: any = useContext(GlobalLoaderContext);
    const [search, setSearch] = useLocationSearch();
    const [query, setQuery] = useState<Query>(new Query());
    const [items, setItems] = useState([]);
    const [suggestions, setSuggestions] = useState<any[]>([]);
    const [filters, setFilters] = useState([]);
    const [categoryPath, setCategoryPath] = useState([]);
    const [stores, setStores] = useState([]);
    const [sorts, setSorts] = useState<any>([]);
    const [checks, setChecks] = useState<any>(checkInit);

    useEffect(() => {
        if (!search) global.navigate('/');
        const q = new Query();
        getItems(q);
    }, [search, setSearch]);


    const getItems = (q: Query) => {
        if (!search) return;
        global.setLoading(true);
        q.query = search.toString();
        const c = checks;

        /***** HTTP REQUESTS JOIN *****/
        join({
            items: http.getItems(q),
            suggestions: http.getSuggestions(q.query),
        }).then(response => {
            const items: any = response.items;
            if (response.suggestions.suggested_queries) setSuggestions(response.suggestions.suggested_queries);

            q.paging = items.paging;
            q.paging.results = Math.ceil(q.paging.total / q.paging.limit );
            q.sort = items.sort;
            q.filters = items.filters;


            /***** CATEGORY PATHS (BREAD CRUMB) *****/
            let pathCat: any;
            if (items.filters[0]) pathCat = items.filters[0].values;
            if (pathCat) setCategoryPath(pathCat[0].path_from_root);


            /***** BUILD OBJECTS FILTERS *****/

            /* OFFICIAL STORES FILTER */
            if (stores.length == 0) {
                let offStores = items.available_filters.filter((e: any) => e.id == 'official_store');
                if (offStores && offStores.length > 0)  setStores(offStores[0].values);
            }

            /***** SET FILTERS CHECKS VALIDATORS *****/
            c['shipping'].show = JSON.stringify(items.available_filters.filter((e: any) => e.id == 'shipping')).includes('fulfillment');
            c['shipping_cost'].show = JSON.stringify(items.available_filters.filter((e: any) => e.id == 'shipping_cost')).includes('free');
            c['SHIPPING_ORIGIN'].show = JSON.stringify(items.available_filters.filter((e: any) => e.id == 'SHIPPING_ORIGIN')).includes('10215069');

            /* ALL FILTERS */
            q.filters = items.filters;

            /***** SET STATES VALUES *****/
            setQuery(q);
            setChecks(c);
            setItems(items.results);
            setFilters(items.available_filters);
            setSorts(items.available_sorts);
            global.setLoading(false);
        })
    }

    function setPage(page: number) {
        const q = query;
        q.paging.offset = page - 1;
        getItems(q);
    }

    function changeSort(key: Key) {
        const q: Query = query;
        q.sort = sorts.filter((s: any) => s.id === key)[0];
        getItems(q);
    }

    function addFilter(filter: any, item?: any) {
        if (!filter) return;
        const q: Query = query;

        if (item) {
            const newFilter = filter;
            newFilter.values = [];
            newFilter.values.push(item);
            q.filters.push(newFilter);
        } else {
            const i: string = filter.id.toString();
            if (checks[i]) {
                let c = checks;
                c[i].checked = true;
                c[i].show = true;
                setChecks(c);
            }

            q.filters.push(filter);
        }
        q.paging.offset = 0;
        getItems(q);
    }

    function removeFilter(filterId: string) {
        const q: Query = query;
        if (checks[filterId]) {
            const c = checks;
            c[filterId].checked = false;
            setChecks(c);
        }
        q.filters = q.filters.filter((f: any) => f.id != filterId);
        q.paging.offset = 0;
        getItems(q);
    }

    return (
        search && <div className="py-3 px-4 md:px-11 mx-auto max-w-[1300px]">

            {/******** RELATED SEARCHES ********/}

            { suggestions.length > 0 &&
            <Suggestions title="Búsquedas relacionadas:" suggestions={suggestions}></Suggestions>
            }

            {/******** CONTENT COLUMNS ********/}
            <div className="grid grid-cols-4 gap-11">
                {/* FIRST COLUMN */}
                <section className="col-span-4 md:col-span-1 ">

                    {/******** BREADCRUMB ********/}
                    <div className="flex justify-between">
                        <ul className="list-none text-sec flex flex-wrap gap-2 text-sm mb-6 max-w-[278px]">
                            {categoryPath && categoryPath.map((path: any, i: number) => {
                                return (
                                    <li key={path.id}>
                                        {path.name}
                                        {i < categoryPath.length - 1 && <i className="las la-angle-right ml-1"></i>}
                                    </li>
                                )
                            })}
                        </ul>
                    </div>

                    <h1 className="font-[600]">{util.capitalCase(search.toString())}</h1>
                    <span className="text-sm font-thin">{util.decimals(query.paging.total)} resultados {checks['shipping'].show.toString()}</span>

                    {/* SELECTED FILTERS */}
                    <ul className="flex flex-wrap gap-3 mt-3 mb-10">
                        {query.filters.map((filter: any) => {
                            return filter.id !== 'category' && !checks[filter.id] && <li key={filter.values[0].id}>
                                <button className="bg-[white] flex items-center text-[13px] px-2 py-1" onClick={() => removeFilter(filter.id)}>
                                    {filter.values[0].name} <i className="las la-times ml-1 text-[#bfbfbf] text-[16px]"></i>
                                </button>
                            </li>
                        })}
                    </ul>

                    {/* TOGGLE FILTER CARDS */}
                    {(checks['shipping'].show || checks['shipping'].checked) &&
                        <Card radius="sm" shadow="none" className="border-1">
                            <CardBody className="p-[15px]">
                                <div className="flex items-center">
                                    <label htmlFor={'full'} className="flex flex-wrap w-[82%] cursor-pointer"
                                           onClick={() => {
                                               document.getElementById('full')?.click()
                                           }}>
                                        <img className="cursor-pointer w-[45px] mr-1"
                                             src={require('../assets/images/full.svg')} alt="envio-full"/><span
                                        className="font-[600] text-[14px]">te da envío gratis</span>
                                        <span className="font-[400] text-[12px]  block w-full">En carritos desde $ 90.000</span>
                                    </label>
                                    <Switch id="full" defaultSelected={checks['shipping'].checked} size="sm"
                                            classNames={{
                                                wrapper: "p-0 h-[20px] w-[36px] overflow-visible",
                                                thumb: cn("w-[20px] h-[20px] shadow-md")
                                            }}
                                            onChange={e => e.target.checked ? addFilter(checks['shipping'].filter) : removeFilter('shipping')}/>
                                </div>
                            </CardBody>
                        </Card>
                    }

                    {(checks['shipping_cost'].show || checks['shipping_cost'].checked) &&
                        <Card radius="sm" shadow="none" className="border-1 mt-2 min-h-[56px]">
                            <CardBody className="p-[15px]">
                                <div className="flex">
                                    <label htmlFor={'full'} className="flex flex-wrap w-[82%] cursor-pointer"
                                           onClick={() => {
                                               document.getElementById('free')?.click()
                                           }}>
                                        <span className="font-[600] text-[14px]">Envío gratis</span>
                                    </label>
                                    <Switch id="free" defaultSelected={checks['shipping_cost'].checked} size="sm"
                                            classNames={{
                                                wrapper: "p-0 h-[20px] w-[36px] overflow-visible",
                                                thumb: cn("w-[20px] h-[20px] shadow-md")
                                            }}
                                            onChange={e => e.target.checked ? addFilter(checks['shipping_cost'].filter) : removeFilter('shipping_cost')}/>
                                </div>
                            </CardBody>
                        </Card>
                    }

                    {(checks['SHIPPING_ORIGIN'].show || checks['SHIPPING_ORIGIN'].checked) &&
                        <Card radius="sm" shadow="none" className="border-1 mt-2">
                            <CardBody className="p-[15px]">
                                <div className="flex items-center">
                                    <label htmlFor={'ci'} className="flex flex-wrap w-[82%] cursor-pointer"
                                           onClick={() => {
                                               document.getElementById('ci')?.click()
                                           }}>
                                        <img className="cursor-pointer w-[165px] mb-2 mr-1"
                                             src={require('../assets/images/ci.svg')} alt="envio-full"/>
                                        <span className="font-[400] text-[12px]  block w-full">Miles de productos del mundo a tu casa</span>
                                    </label>
                                    <Switch id="ci" defaultSelected={checks['SHIPPING_ORIGIN'].checked} size="sm"
                                            classNames={{
                                                wrapper: "p-0 h-[20px] w-[36px] overflow-visible",
                                                thumb: cn("w-[20px] h-[20px] shadow-md")
                                            }}
                                            onChange={e => e.target.checked ? addFilter(checks['SHIPPING_ORIGIN'].filter) : removeFilter('SHIPPING_ORIGIN')}/>
                                </div>
                            </CardBody>
                        </Card>
                    }

                    {/* AVAILABLE FILTERS */}
                    <div className="mt-6">
                        {filters.map((filter: any) => {
                            return filter.id != 'official_store' && filter.id != 'product' && !filter.name.toLowerCase().includes('otr') && !filter.name.toLowerCase().includes('filtro') &&
                                <div key={filter.id} className="my-6">
                                    <h3 className="font-[600] text-[16px] mb-3">{filter.name}</h3>
                                    <ul className="list-none p-0">
                                        {filter.values.map((item: any, i: number) => {
                                            return item && i <= 10 &&
                                                <li key={filter.id + item.id} className="text-[14px] font-[400] text-sec">
                                                    <button onClick={() => {
                                                        addFilter(filter, item)
                                                    }}>
                                                        {item.name}
                                                        <span className="ml-2 text-ter font-thin">({item.results})</span>
                                                    </button>
                                                </li>
                                        })}
                                    </ul>
                                </div>
                        })}
                    </div>
                </section>
                {/* SECOND COLUMN */}
                <section className="col-span-4 md:col-span-3 pb-52">
                    <div className="flex justify-end mb-5 text-sm text-[14px]">
                        <strong>Ordenar por </strong>
                        <Dropdown radius="none" className="p-0 pl-[1px]">
                            <DropdownTrigger>
                                <button className="mx-3">{util.capitalCase(query.sort.name)} <i
                                    className="las la-angle-down text-acc"></i></button>
                            </DropdownTrigger>


                            <DropdownMenu className="p-0" onAction={key => { changeSort(key) }}>
                                <DropdownItem key="new"
                                              className="rounded-none py-3 text-acc border-l-1 border-l-[var(--accent)] pointer-events-none"><strong>{util.capitalCase(query.sort.name)}</strong></DropdownItem>
                                {sorts?.length > 0 && sorts.map((item: any) => {
                                    return (
                                        <DropdownItem key={item.id}
                                                      className="border-t-1 rounded-none py-3">{item.name}</DropdownItem>
                                    )
                                })}
                            </DropdownMenu>
                        </Dropdown>
                    </div>

                    {/* STORES  SLIDER */}
                    <div className="flex gap-9 overflow-scroll mb-9">
                        {stores.map((store: any, i: number) => {
                            return i <= 8 && !store.name.includes('Todas') && (
                                <div key={store.id} className="w-[86px] min-w-[86px] flex flex-wrap justify-center text-center">
                                    <img key={store.id}
                                         className="h-[86px] w-[86px] object-center object-contain rounded-full bg-[#fff]"
                                         src={`https://http2.mlstatic.com/${STORES_IMG[Math.round(Math.random() * (5))]}`}
                                         alt={store.id}/>
                                    <span className="text-[12px] font-[600]">{store.name}</span>
                                </div>
                            )
                        })}
                    </div>

                    {/******** ITEMS ARRAY ITERATION ********/}
                    {items.map((item: any) => {
                        return (
                            <ListItem key={item.id} item={item} util={util}
                                      fullLogo={require('../assets/images/full.svg')}></ListItem>
                        )
                    })}

                    <div className="mt-9">
                    {!global.loading && query.paging.total > query.paging.limit &&
                        <Pagination className=" flex justify-center" showControls radius="sm"
                                    total={query.paging.results} initialPage={query.paging.offset + 1}
                                    onChange={(page) => {
                                        setPage(page)
                                    }}/>
                    }
                    </div>
                </section>
            </div>
        </div>
    )
}

export default Search;