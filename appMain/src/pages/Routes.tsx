import React from 'react';
import {Route, Switch} from "wouter";
import Home from "./Home";
import Search from "./Search";
import NotFound from "./NotFound";
import ItemDetail from "./ItemDetail";

const Routes = () => {
    return (
        <Switch>
            <Route path="/" component={Home}/>
            <Route path="/items" component={Search}/>
            <Route path="/items/:id" component={ItemDetail}/>
            <Route component={NotFound}></Route>
        </Switch>
    );
};

export default Routes;