import React, {useEffect, useLayoutEffect, useState} from 'react';
import {useParams} from "wouter";
import {capitalCase, currency, getDiscount, join} from "../util/utils";
import {http} from "../api/http";
import Suggestions from "components/Suggestions";
import {Button, Card, CardBody} from "@nextui-org/react";
import {Carousel} from "react-responsive-carousel";
import {Rating} from "react-simple-star-rating";

const ItemDetail = () => {
    const params = useParams();
    const [item, setItem] = useState<any>();
    const [detail, setDetail] = useState<any>()
    const [suggestions, setSuggestions] = useState<any[]>([]);

    useEffect(() => {
        getItemDetail();
    }, []);

    useLayoutEffect(() => {
        if (!item) return;
        const images = document.getElementsByClassName('thumb');
        Array.from(images).forEach((el: any) => {
            el.addEventListener('mouseover', () => {
                const elActive = document.querySelector('.thumb.selected');
                if (!el.isEqualNode(elActive)) {
                    el.click();
                }
            })
        })
    }, [item]);

    const getItemDetail = () => {
        const itemId: string = params.id ? params.id : '';
        join({
            item: http.getItem(itemId),
            detail: http.getItemDescription(itemId),
            suggestions: http.getSuggestions('tecnologia'),
        }).then((response: any) => {
            console.clear();
            setItem(response.item);
            setDetail(response.detail);
            if (response.suggestions.suggested_queries) setSuggestions(response.suggestions.suggested_queries);
        })
    }

    return (
        <> {item &&
            <div className="py-3 px-5 xl:px-0  mx-auto max-w-[1184px]">
                {/******** RELATED SEARCHES ********/}
                {suggestions.length > 0 &&
                    <Suggestions suggestions={suggestions} title="También puede interesarte:"></Suggestions>
                }

                <div className="mt-3">
                    <button onClick={() => history.back() } className="hover hover:text-[--accent]">Volver</button>
                </div>
                <Card radius="none" shadow="none" className="border-b-[5px] mb-24">
                    <CardBody className="p-[11px] mb-16">
                        <div className="grid grid-cols-7 gap-5">
                            <div className="col-span-7 md:col-span-5">
                                <div className="grid grid-cols-9 gap-5">
                                    <section className="col-span-9 md:col-span-5">
                                        <Carousel className="item-carousel" autoPlay={false} showThumbs={true}
                                                  thumbWidth={53} transitionTime={500} infiniteLoop={true}
                                                  showStatus={false} showIndicators={false}>
                                            {item.pictures.map((picture: any) => (
                                                <img key={picture.id}
                                                     className="h-[100vw] sm:h-[468px] w-full object-center object-contain"
                                                     src={picture.secure_url} alt={picture.id}/>
                                            ))}
                                        </Carousel>
                                    </section>
                                    <section className="col-span-9 md:col-span-4">
                                        <span className="block text-[14px] text-sec mb-2 mt-5">nuevo | +1000 vendidos</span>
                                        <h1 className="font-[600] text-[22px] leading-6">{capitalCase(item.title)}</h1>
                                        <div className="mt-2">
                                            <span
                                                className="font-[300] text-[14px] mr-2">{item.rate = Math.round(Math.random() * (5))}</span>
                                            <span>
                                                <Rating className="mb-2" emptyColor="#fff" fillColor='#3483fa'
                                                        SVGstorkeWidth={1}
                                                        SVGstrokeColor="#3483fa" size={16} disableFillHover={true}
                                                        readonly={true}
                                                        initialValue={item.rate}/>
                                            </span>
                                            <span
                                                className="font-[300] text-[14px] ml-2">({Math.round(Math.random() * (500))})
                                            </span>
                                        </div>
                                        <div className="mt-2">
                                            {item.original_price &&
                                                <span
                                                    className="text-[16px] font-[300] text-ter line-through block">{currency(item.original_price)}</span>
                                            }
                                            <span
                                                className="font-[300] text-[36px] leading-9">{currency(item.price)}</span>

                                            {item.original_price &&
                                                <span
                                                    className="ml-2 text-succ text-[18px] font-[400]">{getDiscount(item.price, item.original_price)}</span>
                                            }

                                            {item.installments && item.installments.quantity &&
                                                <span
                                                    className="block text-[14px]">en {item.installments.quantity}x {currency(item.installments.amount)}</span>
                                            }
                                            <span className="block font-[400] text-[14px] text-[#2968c8]">Ver los medios de pago</span>
                                            <div className="mt-6">
                                                <span className="font-[16px]">Color: <strong>Amarillo</strong></span>
                                                <div className="mt-2">
                                                    <ul className="list-none p-0 m-0">
                                                        <li className="w-[46px] h-[46px] p-1 mr-2 rounded-[5px] border-2 border-[var(--accent)]">
                                                            <img className="w-full h-full object-center object-cover"
                                                                 src={item.thumbnail} alt={item.thumbnail_id}/>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="mt-9">
                                                <h2 className="font-[600] text-[14px]">Lo que tienes que saber de este
                                                    producto</h2>
                                                <ul className="list-inside list-disc mx-0 mt-2 text-[16px]">
                                                    {item.attributes.map((attr: any, i: number) => (
                                                        i <= 8 && <li key={attr.id}
                                                                      className="marker:text-[12px] text-[14px] mt-2">{attr.name} {attr.value_name}</li>
                                                    ))}
                                                </ul>
                                                <span className="block font-[400] text-[14px] text-[#2968c8]">Ver características</span>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <hr className="m-11"/>
                                <div className="px-11">
                                    <h2 className="text-[24px] font-[400] mb-6">
                                        Descripción
                                    </h2>
                                    <p className="text-[20px] text-sec">
                                        {detail.plain_text}
                                    </p>
                                </div>
                            </div>
                            <div className="col-span-7 md:col-span-2">
                                <div className="border-1 rounded-[5px] p-3">
                                    <div className="mt-2">
                                        {item.shipping.free_shipping &&
                                            <span className="mt-3 text-[14px] font-[300]">
                                            <span className="mr-2 text-succ font-[600]">Envío gratis</span>
                                            a todo el país
                                        </span>
                                        }
                                        {item.shipping.logistic_type == 'fulfillment' &&
                                            <span className="block mt-3 text-sec text-[14px] font-[400] flex">Envíado por
                                            <img className="cursor-pointer w-[45px] ml-1"
                                                 src={require('../assets/images/full.svg')} alt="envio-full"/>
                                        </span>
                                        }
                                        <span className="block font-[400] text-[14px] text-ter">Conoce los tiempos y las formas de envío.</span>
                                        <span className="block font-[400] text-[14px] text-[#2968c8]">Calcular cuándo llega</span>
                                        <h3 className="font-[600] text-[16px] my-6">Stock disponible</h3>
                                        <span
                                            className="block font-[400] text-[16px]">Cantidad: <strong>1 unidad</strong>
                                            <span
                                                className="text-[14px] text-sec ml-2">(+ {item.initial_quantity} disponibles)</span>
                                        </span>
                                        <Button color="primary" disableRipple={true} disableAnimation={true} radius="sm"
                                                className="w-full text-[16px] mt-6">
                                            Comprar ahora
                                        </Button>

                                        <Button radius="sm"
                                                className="w-full text-[16px] mt-2 bg-[#e2edfc] text-[--accent]">
                                            Agregar al carrito
                                        </Button>

                                        <span className="block font-[400] text-[14px] text-[#2968c8] mt-6">Devolución gratis.
                                            <span className="text-[14px] text-sec ml-1">Tienes 30 días desde que lo recibes.</span>
                                        </span>

                                        <span className="block font-[400] text-[14px] text-[#2968c8] mt-6 mb-16">Compra Protegida,
                                            <span className="text-[14px] text-sec ml-1">recibe el producto que esperabas o te devolvemos tu dinero.</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </CardBody>
                </Card>
            </div>
        }</>
    );
}

export default ItemDetail;