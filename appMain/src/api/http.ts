import {Query} from "../models/Query";
import {SITE} from "../util/utils";

export const http =  {

    getItems: async (query: Query): Promise<any> => {
        const paging: any = query.paging;
        let filters: string = `q=${query.query}&limit=${paging.limit}&offset=${paging.offset}&sort=${query.sort.id}`;
        query.filters.forEach((filter: any) => {
            filters = filters + `&${filter.id}=${filter.values[0].id}`;
        })
        return await fetch(`https://api.mercadolibre.com/sites/${SITE ? SITE : 'MCO'}/search?${filters}`).then(response => response.json());
    },

    getItem: async (id: string): Promise<any> => {
        return await fetch(`https://api.mercadolibre.com/items/${id}`).then(response => response.json());
    },

    getItemDescription: async (id: string): Promise<any> => {
        return await fetch(`https://api.mercadolibre.com/items/${id}/description`).then(response => response.json());
    },

    getSuggestions: async (query: string): Promise<any> => {
        const SITE = localStorage.getItem('COUNTRY');
        return await fetch(`https://http2.mlstatic.com/resources/sites/${SITE ? SITE : 'MCO'}/autosuggest?q=${query}`).then(response => response.json());
    },

    getSites: async (): Promise<any> => {
        return await fetch('https://api.mercadolibre.com/sites').then(response => response.json());
    }

}