export class Installment {
    quantity: number;
    amount: number;
    constructor(quantity?: number, amount?: number) {
        this.quantity = quantity ?? 0;
        this.amount = amount ?? 0;
    }
}