# Challenge ML

<div style="text-align: center;">
  <img style="width: 250px; margin-top: 30px; margin-bottom: 50px" src="https://cdn.worldvectorlogo.com/logos/mercadolibre.svg" alt="Mercado Libre Logo">
</div>



## Table of Contents

    Description
    Projects Overview
    Getting Started
    Project Ports
    Technologies
    Contact
    
    
## Description

This repository contains two React microfrontends, "**appMain**" and "**appComponents**", which together form the Challenge ML project.

    
## Projects Overview

### appMain

This is the main project (entry point) of the Challenge ML application. It contains the main modules and handles basic functionalities such as routing.

### appComponents

This project contains reusable components designed to be easily integrated into different parts of the application.



## Getting Started


To run the projects locally, follow these steps:


1. Clone this repository to your local machine.

```bash
git clone https://gitlab.com/dpatarroyo/challenge-ml.git
```

2. Navigate to the root directory of each project (**`appMain`** and **`appComponents`**) in separate terminal windows.

    
```bash
cd appMain
```
and

```bash
cd appComponents
```

3. Install dependencies for each project by running 
    
```bash
npm install
```

4. Start each project by running

```bash
npm start
```

## Project Ports

- **appMain**: Runs on port 3000.
- **appComponents**: Runs on port 3001.



## Technologies

- **React**: JavaScript library for building user interfaces.
- **Tailwind CSS**: CSS framework with utility classes for implementing custom web designs.
- **Nextui**: Customizable React component library designed for modern application development.


## Contact

For any inquiries or feedback regarding Challenge ML, please contact [David Santiago Patarroyo](mailto:davidspatarroyo@gmail.com).


