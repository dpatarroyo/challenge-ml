import React, {useEffect, useLayoutEffect} from 'react'
import ReactDOM from 'react-dom/client'
import './index.scss'
import {NextUIProvider} from "@nextui-org/react";
import {useLocation} from "wouter";
import Header from "components/Header";
import Routes from "./pages/Routes";
import {I18nextProvider} from "react-i18next";
import i18next from "./util/I18n";

export const GlobalLoaderContext = React.createContext({});

const App = () => {
    const [menu, setMenu] = React.useState([]);
    const [, navigate] = useLocation();
    const [height, setHeight] = React.useState('100%');
    const [loading, setLoading] = React.useState(false);
    const [clang, setClang] = React.useState<string>('es');

    useEffect(() => {
        let header = document.querySelector('header');
        if (header) setHeight(`${window.innerHeight - header.clientHeight}px`);
        getMenu();
        window.addEventListener('resize', function() {
            if (header) setHeight(`${window.innerHeight - header.clientHeight}px`);
        }, true);
        window.addEventListener('changeLang', () => {
            setMenu([]);
            setClang(i18next.language);
        }, true);
    }, [])

    useLayoutEffect(() => {
        getMenu();
    }, [clang]);


    const getMenu = () => {
        const _menu: any = [];
        const a: any = i18next.getDataByLanguage(i18next.language);
        Object.keys(a.global.menu).forEach(key => {
            _menu.push(a.global.menu[key]);
        })
        setMenu(_menu);
    }

    return (
        <NextUIProvider>
            <I18nextProvider i18n={i18next}>
                <GlobalLoaderContext.Provider value={{loading: loading, setLoading: setLoading, navigate: navigate}}>

                    <main className="h-screen w-full overflow-hidden">
                        { menu.length > 0 &&
                            <Header logo={require('./assets/images/logo.svg')} menu={menu}></Header>
                        }
                        <div className={`relative z-0 overflow-scroll w-full`} style={{height: height}}>
                            <Routes></Routes>
                        </div>
                    </main>
                    {loading &&
                        <div className="loader fixed top-0 right-0 w-full h-screen flex justify-center items-center">
                            <img className="w-28 object-contain" src={require('./assets/images/loader.gif')} alt="loader"/>
                        </div>
                    }
                </GlobalLoaderContext.Provider>
            </I18nextProvider>
        </NextUIProvider>
    )
}
const rootElement = document.getElementById('app')
if (!rootElement) throw new Error('Failed to find the root element')

const root = ReactDOM.createRoot(rootElement as HTMLElement)

root.render(<App/>)
