var callJoin = false;

export var SITE = localStorage.getItem('SITE');

export const saveSite = (site: string) => {
    localStorage.setItem('SITE', site);
    SITE = site;
}

export const removeSite = () => {
    localStorage.removeItem('SITE');
    SITE = null;
}


export const capitalCase = (text: string) => {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

export const webpThumbnail = (url: string) => {
    return url.replace('I.jpg', 'V.webp');
}

export const currency = (value: any) => {
    if (!value) return '';
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'decimal',
        maximumFractionDigits: 0,
    });
    return `$ ${formatter.format(value).replaceAll(',', '.')}`;
}

export const decimals = (value: any) => {
    if (!value) return '';
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'decimal',
        maximumFractionDigits: 0,
    });
    return formatter.format(value).replaceAll(',', '.');
}

export const getDiscount = (price: number, originaPrice: number) => {
    return `${Math.round(100 - ((price * 100) / originaPrice))}% OFF`;
}

export const join = (requestsList: any) => {
    if (callJoin) return new Promise(() => null);
    callJoin = true;
    let requests: any[] = [];
    let keys: any[] = [];
    let response: any = Object.assign({}, requestsList);
    Object.keys(requestsList).forEach((key: string) => {
        requests.push(requestsList[key]);
        keys.push(key);
    });
    return Promise.all(requests).then(promises => {
        promises.forEach((promise, index) => {
            response[keys[index]] = promise;
        });
        callJoin = false;
        return response;
    })
}