import i18next from "i18next";
import es from "../assets/i18n/es.json";
import en from "../assets/i18n/en.json";



i18next.init({
    interpolation: { escapeValue: false },
    lng: "es",
    resources: {
        es: {
            global: es,
        },
        en: {
            global: en,
        }
    }
})

export default i18next;

export const changeLang = (i18n: any) => {
    i18n.changeLanguage(i18n.language == 'es' ? 'en' : 'es').then(() => {
        window.dispatchEvent(new Event('changeLang'));
    });
}