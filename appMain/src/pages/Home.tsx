import React, {useEffect} from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from "react-responsive-carousel";
import {Button, Modal, ModalBody, ModalContent} from "@nextui-org/react";
import {http} from "../api/http";
import {removeSite, saveSite, SITE} from "../util/utils";
import {changeLang} from "../util/I18n";
import {useTranslation} from "react-i18next";

const FLAGS = {
    'Chile': 'cl',
    'El Salvador': 'sv',
    'Paraguay': 'py',
    'Brasil': 'br',
    'Honduras': 'hn',
    'Colombia': 'co',
    'Cuba': 'cu',
    'Ecuador': 'ec',
    'Mexico': 'mx',
    'Costa Rica': 'cr',
    'Guatemala': 'gt',
    'Panamá': 'pa',
    'Bolivia': 'bo',
    'Dominicana': 'do',
    'Argentina': 'ar',
    'Nicaragua': 'nc',
    'Venezuela': 've',
    'Perú': 'pe',
    'Uruguay': 'uy'
}

const Home = () => {
    const [showCountries, setShowCountries] = React.useState(false);
    const [sites, setSites] = React.useState([]);
    const [t, i18n] = useTranslation("global");

    useEffect(() => {
        if (!SITE) {
            getCountries();
        }
    }, []);

    const getCountries = () => {
        http.getSites().then(sites => {
            setSites(sites);
            setShowCountries(true);
        });
    }

    const setCountry = (country: string) => {
        saveSite(country)
        setShowCountries(false);
    }

    const changeCountry = () => {
        removeSite();
        getCountries();
    }


    return (
        <div className="mx-auto max-w-[1600px]">
            <Carousel autoPlay={true} showThumbs={false} interval={3000} transitionTime={500} stopOnHover={true}
                      infiniteLoop={true} showStatus={false}>
                <img className="h-[200px] md:h-[300px] lg:h-[350px] object-bottom object-cover"
                     src="https://http2.mlstatic.com/storage/splinter-admin/o:f_webp,q_auto:best/1715099248639-desktopsinbanco.png"
                     alt="home"/>
                <img className="h-[200px] md:h-[300px] lg:h-[350px] object-bottom object-cover"
                     src="https://http2.mlstatic.com/storage/splinter-admin/o:f_webp,q_auto:best/1715108890392-desktopsinbanco6.png"
                     alt="home"/>
                <img className="h-[200px] md:h-[300px] lg:h-[350px] object-bottom object-cover"
                     src="https://http2.mlstatic.com/storage/splinter-admin/o:f_webp,q_auto:best/1715110984599-desktopsinbanco7.png"
                     alt="home"/>
                <img className="h-[200px] md:h-[300px] lg:h-[350px] object-bottom object-cover"
                     src="https://http2.mlstatic.com/storage/splinter-admin/o:f_webp,q_auto:best/1715099344212-desktopsinbanco1.png"
                     alt="home"/>
            </Carousel>

            <div className="flex flex-wrap justify-center mt-6 gap-3 p-4">
                <Button color="primary" disableAnimation={true} radius="sm"
                        className="w-full md:max-w-[300px] text-[16px]" onClick={()=> changeCountry()}>
                    {t('change_country')}
                </Button>

                <Button color="primary" disableAnimation={true} radius="sm"
                        className="w-full md:max-w-[300px] text-[16px]" onClick={()=> changeLang(i18n)}>
                    {t('change_lang')}
                </Button>
            </div>

            <Modal className="px-4 pt-24 pb-6 bg-[var(--primary)]" size="full" hideCloseButton={true} isOpen={showCountries} onOpenChange={() => setShowCountries(!showCountries)} isDismissable={false}>
                <ModalContent>
                    <ModalBody>
                        <ul className="mt-6">
                            {sites.map((site: any) => (
                                <li key={site.id}>
                                    <button className="flex items-center mt-2">
                                        {/*@ts-ignore*/}
                                        <img src={`https://flagcdn.com/${FLAGS[site.name]}.svg`}
                                            width="30"
                                            alt={site.name}/>
                                        <span className="text-acc ml-3" onClick={() => setCountry(site.id)}>{site.name}</span>
                                    </button>
                                </li>
                            ))}
                        </ul>
                    </ModalBody>
                </ModalContent>
            </Modal>

        </div>
    )
}

export default Home;