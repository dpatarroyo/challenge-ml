import {Installment} from "./Installment";

export class Item {
    id: string;
    thumbnail: string;
    title: string;
    store: string;
    price: number;
    originalPrice: number;
    discount: number;
    rate: number;
    pictures: any;
    attributes: any;
    isFreeShipping: boolean;
    isFullShipping: boolean;
    installments?: Installment;
    constructor(
        id?: string,
        title?: string,
        store?: any,
        thumbnail?: string,
        price?: number,
        originalPrice?: number,
        discount?: number,
        rate?: number,
        pictures?: any,
        attributes?: any,
        isFreeShipping?: boolean,
        isFullShipping?: boolean,
        Installments?: Installment,
    ) {
        this.id = id ?? '';
        this.thumbnail = thumbnail ?? 'MCO';
        this.title = title ?? '';
        this.store = store ?? '';
        this.price = price ?? 0;
        this.originalPrice = originalPrice ?? 0;
        this.discount = discount ?? 0;
        this.rate = rate ?? 0;
        this.pictures = pictures ?? [];
        this.attributes = attributes ?? [];
        this.isFreeShipping = isFreeShipping == true;
        this.isFullShipping = isFullShipping == true;
        this.installments = Installments ?? new Installment(Installments)
    }
}