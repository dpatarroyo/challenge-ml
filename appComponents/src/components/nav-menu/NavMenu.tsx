import './navmenu.scss'
import React from 'react'

const NavMenu = (props: any) => {

    return (
        props &&
            <div className="w-full flex flex-wrap gap-2 sm:justify-between justify-end mt-4 z-10">
                <nav className="w-full md:w-auto hidden sm:block">
                    <ul className="header-menu">
                        {props.menu.map((item: any) => (
                            <li key={item}>{item}</li>
                        ))}
                    </ul>
                </nav>
                <nav className={`drawer sm:hidden fixed ${props.showDrawer ? 'active' : ''}`}>
                    <div className="w-full h-32 drawer-header"></div>
                    <ul className="mt-10 p-4">
                        {props.menu.map((item: any) => (
                            <li key={item}>{item}</li>
                        ))}
                    </ul>
                </nav>
                <nav className="w-auto">
                    <ul className="header-menu">
                        <li>Crea tu cuenta</li>
                        <li>Mis compras</li>
                        <li><i className="las la-shopping-cart"></i></li>
                    </ul>
                </nav>
            </div>
    )
}
export default NavMenu;